import axios from 'axios'

export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_SUCCESS = "FETCH_SUCCESS";

export const POST_ORDER_REQUEST = "POST_ORDER_REQUEST";
export const POST_ORDER_SUCCESS = "POST_ORDER_SUCCESS";

export const ADD_TO_ORDER = 'ADD_TO_ORDER';
export const REMOVE_TO_ORDER = 'REMOVE_TO_ORDER';

export const TOGLE_MODAL = 'TOGLE_MODAL';




export const fetchDishesSuccess = (dishes) => ({type: FETCH_SUCCESS, dishes});
export const postOrderSuccess = () => ({type: POST_ORDER_SUCCESS});

export const addToOrderBasket = (order) => ({type: ADD_TO_ORDER, order});
export const removeOrder  = (id) => ({type: REMOVE_TO_ORDER, id});

export const toggleModal = () => ({type: TOGLE_MODAL});


export const getToDishes = () => {
    return dispatch => {
        axios.get('https://burger-project-habi.firebaseio.com/Dishes.json').then(response => {
            const dishes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            dispatch(fetchDishesSuccess(dishes))
        })
    }
};

export const postToOrder = orderData => {
    return dispatch => {
        axios.post('https://burger-project-habi.firebaseio.com/Orders.json', orderData).then(() => {
            dispatch(postOrderSuccess())
        })
    }
};
export const addToOrder = (order) => {
  return dispatch => {
      dispatch(addToOrderBasket(order))
  }
};

export const removeToOrder = (order,  price) => {
    return dispatch => {
       dispatch(removeOrder(order))
    }
};

export const togleToModal = () => {
    return dispatch => {
        dispatch(toggleModal())
    }
};

