import {ADD_TO_ORDER, FETCH_SUCCESS, POST_ORDER_SUCCESS, REMOVE_TO_ORDER, TOGLE_MODAL} from "./actions";


const initialState = {
        menu: [],
        basket: [],
        modalVisible: false,
        totalPrice: 0,
        delivery: 150
};

const reducer = (state = initialState, action) => {

      switch (action.type) {
          case FETCH_SUCCESS:
              return {
                  ...state,
                  menu: action.dishes
              };
          case ADD_TO_ORDER:
              let order;
              if (state.basket[action.order.name]) {
                  order = {...action.order, amount: state.basket[action.order.name].amount + 1}
              } else {
                  order = {...action.order, amount: 1}
              }

              const orders = {...state.basket, [action.order.name]: order};
              console.log(orders);

              return {
                  ...state,
                 basket: orders,
                 totalPrice: state.totalPrice  +=  parseInt(order.price),
              };
          case REMOVE_TO_ORDER:

              let basketOrder;
              const basket = Object.keys({...state.basket}).map(id => {
                 return {...state.basket[id], id}
              });

              basketOrder = basket;
              const index = basketOrder.findIndex(item => item.name === action.id);
              basketOrder.splice(index, 1);

              return{
                ...state,
                  basket: basketOrder,
                  totalPrice: state.totalPrice - state.totalPrice
              };
          case POST_ORDER_SUCCESS:
              return {
                ...state,
                modalVisible: false,
                basket: [],
                totalPrice: 0
              };
          case TOGLE_MODAL:
              return {
                 ...state,
                  modalVisible: !state.modalVisible
              };
          default:
              return state
      }

};


export default reducer;