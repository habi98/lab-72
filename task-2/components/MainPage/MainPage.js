import React from 'react';

import {FlatList, Image, Modal, StyleSheet, Text, TouchableOpacity, View, Button} from "react-native";
import {addToOrder, getToDishes, postToOrder, removeToOrder, togleToModal} from "../../store/actions";
import {connect} from "react-redux";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 30,
    },
    containerText: {
        flexDirection: 'row',
        paddingLeft: 10,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 10,

    },
    text: {
        flex: 1,
        fontSize: 18
    },
    containerDish: {
        paddingHorizontal: 10,
    },
    containerView: {
        padding: 20,
        marginTop: 10,
        backgroundColor: 'lightblue',
        flexDirection: 'row'
    },
    Image: {
        width: 50,
        height: 50,
        marginRight: 10,
    },
    textPrice: {
        flex: 1,
    },
    price: {
        textAlign: 'right'
    },
    totalContainer: {
        borderTopColor: 'black',
        borderTopWidth: 1,
        display: 'flex',
        flexDirection: 'row'
   },
    orderPrice: {
        padding: 20,
        width: '50%',
    },
    Checkout: {
        padding: 20,
        width: '50%',
    },
    Button: {
        backgroundColor: 'lightblue',
    },
    orderContainer: {
        height: 483,
        backgroundColor: '#fff',
        paddingHorizontal: 20,
    },
    order: {
        paddingBottom: 10,
        marginBottom: 20
    },
    dish: {
      marginBottom: 10,
      display: 'flex',
      flexDirection: 'row'
    },
    buttonDelete: {
      width: 30,
      fontSize: 10,
      marginLeft: 20
    },
    buttonContainerDelete: {
        flex: 1,
    },
    title: {
      fontSize: 23
    },
    buttonCancel: {
        textAlign: 'center',
        backgroundColor: 'lightblue',
        borderWidth: 1
    },
    buttonOrder: {
        textAlign: 'center',
        backgroundColor: 'lightblue',
        borderWidth: 1,
        marginTop: 5
    },
    button: {
        padding: 5,
        textAlign: 'center',
        fontSize: 20
    }

});


class MainPage extends React.Component {
    componentDidMount() {
        this.props.getDishes();
    }

    orderHandler = (event) => {
        event.preventDefault();


        const order = Object.keys(this.props.basket).map(name => {
            return {...this.props.basket[name], name}
        });
        let Order = order;

        const dataOrder = Order.map(order => {
            return  {name: order.name, amount: order.amount}
        });
        console.log(dataOrder);
        this.props.postToOrder(dataOrder)
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerText}>
                    <Text style={styles.text}>Turtle Pizza</Text>
                </View>

                <FlatList
                    data={this.props.menu}
                    keyExtractor={(item) => item.id}
                    renderItem={({item}) => {
                        return (
                            <TouchableOpacity onPress={()=> this.props.addToOrder({name: item.name, price: item.price, id: item.id})} style={styles.containerDish }>
                                <View style={styles.containerView}>
                                    <Image
                                        source={{uri: item.image}}
                                        style={styles.Image}
                                    />
                                    <Text>{item.name}</Text>
                                    <View style={styles.textPrice}>
                                        <Text style={styles.price}>{item.price} KGS</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    }}

                />
                <View style={styles.totalContainer}>
                    <View style={styles.orderPrice}>
                        <Text>Order total {this.props.totalPrice}</Text>
                    </View>
                    <View style={styles.Checkout}>
                        <TouchableOpacity style={styles.Button } onPress={this.props.togleToModal}>
                            <View >
                                <Text style={{textAlign: 'center', padding: 5}}>Checkout</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>


                <Modal
                    animationType="slide"
                    visible={this.props.modalVisible}
                    onRequestClose={this.props.togleToModal}

                >
                    <View style={styles.orderContainer}>
                        <View style={{paddingBottom: 20, paddingTop: 10}}>
                            <Text style={styles.title}>Your order:</Text>
                        </View>
                        <View style={styles.order}>
                            {Object.keys(this.props.basket).map((dish,id) => {
                                return (
                                    <View style={styles.dish} key={id}>
                                        <Text style={{marginRight: 20}} style={{textAlignVertical: 'center', textAlign: 'right'}}>{this.props.basket[dish].name} x{this.props.basket[dish].amount} {this.props.basket[dish].price} KGS</Text>
                                        <View style={styles.buttonContainerDelete}>
                                            <TouchableOpacity style={styles.buttonDelete}>
                                                <Text onPress={() => this.props.removeToOrder(this.props.basket[dish].name, this.props.price )} style={{ textAlign: 'center', fontSize: 18}}>x</Text>
                                            </TouchableOpacity>
                                        </View>

                                    </View>
                                )
                            })}
                        </View>

                        <View >
                            <Text>Delivery {this.props.delivery} KGS</Text>
                            {this.props.totalPrice ? <Text>Total {this.props.delivery + this.props.totalPrice} KGS</Text>: <Text>Total {this.props.totalPrice} KGS</Text>}
                        </View>

                    </View>
                    <View>
                            <TouchableOpacity onPress={this.props.togleToModal} style={styles.buttonCancel}>
                                <Text style={styles.button}>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.orderHandler} style={styles.buttonOrder}>
                                <Text style={styles.button}>Order</Text>
                            </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        )
    }
}

const mapStateToProps = state =>({
    menu: state.menu,
    basket: state.basket,
    modalVisible: state.modalVisible,
    totalPrice: state.totalPrice,
    delivery: state.delivery

});


const mapDispachToProps = dispatch => ({
    getDishes: () => dispatch(getToDishes()),
    addToOrder: (order) => dispatch(addToOrder(order)),
    togleToModal: () => dispatch(togleToModal()),
    removeToOrder: (id, price) => dispatch(removeToOrder(id, price)),
    postToOrder: (orderData) => dispatch(postToOrder(orderData))
});

export default connect(mapStateToProps, mapDispachToProps)(MainPage)




