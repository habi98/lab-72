import React from 'react';
import { StyleSheet, View, Text,FlatList, TouchableOpacity, Image, Modal } from 'react-native';


import reducer from './store/reducer'
import {createStore, applyMiddleware,} from 'redux';
import {Provider} from 'react-redux'
import thunkMiddLeware from 'redux-thunk'

import MainPage from "./components/MainPage/MainPage";


class App extends React.Component {
    render() {
        return (
            <MainPage/>
        );
    }

}



const store =createStore(reducer, applyMiddleware(thunkMiddLeware));

const Index = () => (
    <Provider store={store}>
        <App/>
    </Provider>
);

export default Index;
