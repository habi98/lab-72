import React from 'react';
import {Container, Navbar} from "reactstrap";
import Logo from "../Logo/Logo";
import NavigationNav from "../Navigation/NavigationNav/NavigationNav";

const Header = (props) => {
    return (
        <Navbar  className="navbar navbar-dark bg-primary"  expand="md">
            <Container>
                <Logo />
                <NavigationNav/>
            </Container>
        </Navbar>
    );
};

export default Header;