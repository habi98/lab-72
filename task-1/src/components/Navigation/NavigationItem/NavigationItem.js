import React, {Fragment} from 'react';
import {NavItem, NavLink} from "reactstrap";

import {NavLink as RouterNavLink} from "react-router-dom";

const NavigationItem = () => {
    return (
        <Fragment>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/">Dishes</NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/orders">Orders</NavLink>
            </NavItem>
        </Fragment>
    );
};

export default NavigationItem;