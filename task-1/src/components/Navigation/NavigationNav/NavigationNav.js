import React from 'react';
import {Collapse, Nav} from "reactstrap";
import NavigationItem from "../NavigationItem/NavigationItem";

const NavigationNav = () => {
    return (
        <Collapse navbar>
            <Nav className="ml-auto" navbar>
                <NavigationItem/>
            </Nav>
        </Collapse>
    );
};

export default NavigationNav;