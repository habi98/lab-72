import React, {Fragment} from 'react';
import './Modal.css'
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <Fragment>
            <Backdrop close={props.close} show={props.modalShow} modalCancel={props.cancel}/>
            <div className="Modal" style={{transform: props.modalShow ? 'translateY(0)': 'translateY(-100vh)',
                opacity: props.modalShow ? '1' : 0}}>
                {props.children}
            </div>
        </Fragment>
    );
};

export default Modal;