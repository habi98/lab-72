import React from 'react';

import './Backdrop.css';

const Backdrop = props => (
    props.show ? <div onClick={props.close} className="Backdrop">s</div>: null
);

export default Backdrop;