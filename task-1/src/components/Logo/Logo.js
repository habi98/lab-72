import React, {Fragment} from 'react';
import {NavbarBrand, NavbarToggler} from "reactstrap";

const Logo = (props) => (
    <Fragment>
    <NavbarBrand href="/">Turtle Pizza Admin</NavbarBrand>
    <NavbarToggler />
    </Fragment>
);


export default Logo;