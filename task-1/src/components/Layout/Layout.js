import React, {Fragment} from 'react';
import Toolbar from "../Toolbar/Toolbar";
import {Container} from "reactstrap";

const Layout = ({children}) => {
    return (
        <Fragment>
            <Toolbar />
                <Container>
                    {children}
                </Container>
        </Fragment>
    );
};

export default Layout;