import {ORDER_SUCCESS} from "./actionsType";

import axios from '../../axios-dishes'


export const orderSuccess = (orders) => ({type: ORDER_SUCCESS, orders});


export const getOrder = () => {
   return dispatch => {
       axios.get('Orders.json').then(response => {

           const orders = Object.keys(response.data).map((id,) => {
               return {...response.data[id], id,}
           });

           dispatch(orderSuccess(orders))
       })
   }
};