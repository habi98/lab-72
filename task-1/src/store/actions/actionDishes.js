import axios from '../../axios-dishes';
import {ADD_NEW_DISHES, CLOSE_MODAL, DISHES_REQUEST, DISHES_SUCCESS, FETCH_DISHES_SUCCESS} from "./actionsType";

export const addNewDishes = () => ({type: ADD_NEW_DISHES});
export const closeModal = () => ({type: CLOSE_MODAL});

export const dishRequest = () => ({type: DISHES_REQUEST});
export const dishSuccess = () => ({type: DISHES_SUCCESS});

export const fetchDishesSuccess = (dishes) => ({type: FETCH_DISHES_SUCCESS, dishes});


export const addDishes = () => {
    return dispatch => {
        dispatch(addNewDishes())
    }
};

export const modalClose = () => {
    return dispatch => {
        dispatch(closeModal())
    }
};

export const getDishes = () => {
    return dispatch => {
        axios.get('Dishes.json').then(response => {
            const dishes = Object.keys(response.data).map(id => {
               return {...response.data[id], id}
            });
           dispatch(fetchDishesSuccess(dishes))
        })
    }
};


export const addToDish = (dataDishes) => {
    return dispatch => {
        dispatch(dishRequest());
       axios.post('Dishes.json', dataDishes).then(() => {
           dispatch(dishSuccess());
           dispatch(getDishes())
       })
    }
};

export const deleteDish = id => {
    return dispatch => {
       axios.delete("Dishes/" + id + ".json").then(() => {
           dispatch(getDishes())
       })
    }
};



