import {
    ADD_NEW_DISHES,
    CLOSE_MODAL,
    DISHES_REQUEST,
    DISHES_SUCCESS,
    FETCH_DISHES_SUCCESS
} from "../actions/actionsType";


const initialState = {
    dishes: [],
    show: false,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_NEW_DISHES:
            return {
                ...state,
                show: true
            };
        case CLOSE_MODAL:
            return {...state, show: false};
        case DISHES_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case DISHES_SUCCESS:
            return{
                ...state,
                loading: false,
                show: false
            };
        case FETCH_DISHES_SUCCESS:
            return {
                ...state,
                dishes: action.dishes
            };
        default:
            return state
    }

};


export default reducer;