import {ORDER_SUCCESS} from "../actions/actionsType";


const initialState = {
    orders: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ORDER_SUCCESS:
            return {
                ...state,
                orders: action.orders
            };

        default:
            return state
    }


};


export default reducer;