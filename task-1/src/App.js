import React, { Component } from 'react';
import './App.css';
import {Route} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./containers/Orders/Orders";

class App extends Component {
    state = {
        collapsed: true
    };

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

  render() {
    return (
          <Layout>
            <Route path="/" exact component={Dishes}/>
            <Route path="/orders" component={Orders}/>
          </Layout>
    );
  }
}

export default App;
