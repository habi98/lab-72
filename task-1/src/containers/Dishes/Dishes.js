import React, {Component, Fragment} from 'react';
import Row from "reactstrap/es/Row";
import {Button, Card, CardBody, CardColumns, CardImg, CardSubtitle,  CardTitle, Col,} from "reactstrap";
import AddFormNewDishes from "../AddFormNewDishes/AddFormNewDishes";
import Modal from "../../components/UI/Modal/Modal";
import {connect} from "react-redux";
import {addNewDishes, closeModal, deleteDish, getDishes} from "../../store/actions/actionDishes";

class Dishes extends Component {

   componentDidMount() {
       this.props.getDishes()
   }



   deliteHandler = id => {
       console.log(id)
   };
    render() {
        return (
            <Fragment>
            <Modal close={this.props.modalClose} modalShow={this.props.show}>
              <AddFormNewDishes/>
            </Modal>
           <Row className="mt-4">
               <Col sm={8} >
                   <h3>Dishes</h3>
               </Col>
               <Col sm={4} className="text-right">
                   <Button color="success" onClick={this.props.addDishes}>Add new dish</Button>
               </Col>
           </Row>
                <CardColumns className="pt-5">
                    {this.props.dishes.map((dish, id) => {
                        return (
                            <Card className="p-4" key={id}>
                                <CardImg top width="100%" src={dish.image} alt="Card image cap" />
                                <CardBody>
                                    <CardTitle>{dish.name}</CardTitle>
                                    <CardSubtitle><p>{dish.price} KGS</p></CardSubtitle>
                                    <Button onClick={() => this.props.deleteDish(dish.id)}>Delite</Button>
                                    <Button className="ml-1">Edit</Button>
                                </CardBody>
                            </Card>

                        )
                    })}
                </CardColumns>
            </Fragment>
        );
    }
}


const mapStateToProps = state =>({
    dishes: state.dish.dishes,
    show: state.dish.show

});


const mapDispachToProps = dispatch => ({
     addDishes: () => dispatch(addNewDishes()),
     getDishes: () => dispatch(getDishes()),
     modalClose: () => dispatch(closeModal()),
     deleteDish: (id) => dispatch(deleteDish(id))
});

export default connect(mapStateToProps, mapDispachToProps)(Dishes);