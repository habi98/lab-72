import React, {Component} from 'react';
import {Table} from "reactstrap";
import {connect} from "react-redux";
import {getOrder} from "../../store/actions/actionOrders";

class Orders extends Component {
    componentDidMount() {
        this.props.getOrder()
    }


    render() {
        console.log(this.props.orders);
        return (
            <Table bordered>
                <tbody>

                {Object.keys(this.props.orders).map((order, id) => {
                    return (
                        <tr key={id}>
                            <th scope="row">{this.props.orders[order][id].amount} X</th>
                            <td>{this.props.orders[order][id].name}</td>
                        </tr>
                    )
                })}

                </tbody>
            </Table>
        );
    }
}

const mapStateToProps = state =>({
    orders: state.order.orders

});


const mapDispachToProps = dispatch => ({
    getOrder: () => dispatch(getOrder())
});

export default connect(mapStateToProps,mapDispachToProps )(Orders);