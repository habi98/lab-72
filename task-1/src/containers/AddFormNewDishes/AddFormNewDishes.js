import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {connect} from "react-redux";
import {addToDish} from "../../store/actions/actionDishes";
import Spinner from "../../components/UI/Spinner/Spinner";

class AddFormNewDishes extends Component {
    state = {
        name: '',
        price: '',
        image: ''
    };

    valueChange = (event) => {
        const name = event.target.name;
        this.setState({[name]: event.target.value})
    };

    dishesHandler = (event) => {
        event.preventDefault();
        const dataDishes = {...this.state}

        this.props.addDish(dataDishes)
    };


    render() {
        let form = (
            <Form onSubmit={this.dishesHandler}>
                <FormGroup row>
                    <Label sm={2}>Name</Label>
                    <Col sm={10}>
                        <Input value={this.state.name} onChange={this.valueChange} type="text" name="name" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label  sm={2}>Price</Label>
                    <Col sm={10}>
                        <Input value={this.state.price} onChange={this.valueChange} type="text" name="price" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2}>Image</Label>
                    <Col sm={10}>
                        <Input value={this.state.image} onChange={this.valueChange} type="text" name="image" />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col  sm={{size: 10, offset: 2}}>
                        <Button color="success">Clear order</Button>
                    </Col>
                </FormGroup>
            </Form>
        );

        if (this.props.loading) {
            form = <Spinner/>
        }

        return (
            form
        );
    }
}

const mapStateToProps = state =>({
    loading: state.dish.loading
});

const mapDispachToProps = dispatch => ({
    addDish: (dataDishes) => dispatch(addToDish(dataDishes))
});


export default connect(mapStateToProps ,mapDispachToProps)(AddFormNewDishes);